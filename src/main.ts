import {defineCustomElement} from 'vue'
import App from './App.vue'

const element = defineCustomElement(App);
window.customElements.define('demo-component', element)


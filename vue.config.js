const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  css: {
    extract: true // This is necessary to also be able to inject the css in "serve" mode
  },
  pages: {
    'demo-component': {
      inject: false, //Disable automatic webpack script/css inclusion, do it explicitly in index.html

      entry: 'src/main.ts',
      title: 'Inject CSS demo',
      template: 'public/index.html',
      filename: 'index.html',
    }
  }
})
